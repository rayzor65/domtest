//
//  ArticleService.h
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleService : NSObject

@property (strong, nonatomic) NSMutableArray *articles;

- (void)fetchRawArticles;
- (NSMutableArray *)getArticles;
- (UIImage *)getImageFromURL:(NSString *)fileURL;    

@end

//
//  ViewController.h
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *articles;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end

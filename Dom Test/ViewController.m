//
//  ViewController.m
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import "ViewController.h"
#import "ArticleService.h"
#import "ArticleCell.h"
#import "Article.h"
#import "TSMiniWebBrowser.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *newArticleNotification = @"NewArticlesNotification";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(setArticles:)
     name:newArticleNotification
     object:nil];

    NSString *articlesFetched = @"ArticlesFetched";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(removeIndicator)
     name:articlesFetched
     object:nil];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 190;

    [self refresh];
    // add to canvas
    [self.view addSubview:_tableView];
    [self addToolbarButtons];
}

- (void)initIndicator
{
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    _indicator.center = self.view.center;
    [_tableView addSubview:_indicator];
    [_tableView bringSubviewToFront:_indicator];
    [_indicator startAnimating];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [_articles count];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ArticleCell";
    
    ArticleCell *cell = (ArticleCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    } else {
        cell.imageViewInCell.image = [UIImage imageNamed:@"wait.png"];
    }
    
    Article *article = [_articles objectAtIndex:indexPath.row];
    cell.titleLabel.text = article.headLine;
    cell.descriptionLabel.text = article.slugLine;
    
    if ( ! [article.thumbnailImageHref isEqual:[NSNull null]]) {
        if (article.imageLoaded) {
            cell.imageViewInCell.image = article.articleImage;
        } else {
            NSLog(@"%@", article.thumbnailImageHref);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [self loadImage:indexPath];
            });
        }
    }
    
    return cell;
}

- (void)loadImage:(NSIndexPath *)indexPath
{
    Article *article = [_articles objectAtIndex:indexPath.row];
    ArticleService *articleService = [[ArticleService alloc] init];
    ArticleCell *cell = (ArticleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    ((Article *) [_articles objectAtIndex:indexPath.row]).imageLoaded = YES;

    article.articleImage = [articleService getImageFromURL:article.thumbnailImageHref];
    cell.imageViewInCell.image = article.articleImage;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Article *article = [_articles objectAtIndex:indexPath.row];
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:article.webHref]];
    webBrowser.mode = TSMiniWebBrowserModeNavigation;
    [self.navigationController pushViewController:webBrowser animated:YES];
}

- (void)setArticles:(NSNotification *)notification
{
    NSString *key = @"Articles";
    NSDictionary *dictionary = [notification userInfo];
    NSMutableArray *articles = [dictionary valueForKey:key];
    _articles = articles;
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addToolbarButtons
{
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    self.navigationItem.rightBarButtonItem = refreshButton;
}

- (void)refresh
{
    [self initIndicator];
    [_articles removeAllObjects];
    [self.tableView reloadData];
    
    ArticleService *articleService = [[ArticleService alloc] init];
    [articleService fetchRawArticles];
    [self.tableView reloadData];
}

- (void)removeIndicator
{
    [_indicator removeFromSuperview];
}

@end

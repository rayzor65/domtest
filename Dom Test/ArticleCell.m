//
//  ArticleCell.m
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 300, 30)];
        _titleLabel.textColor = [UIColor blueColor];
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 30, 300, 30)];
        _descriptionLabel.textColor = [UIColor blackColor];
        [self addSubview:_titleLabel];
        [self addSubview:_descriptionLabel];
        
        _imageViewInCell = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 150, 90)];
        [self addSubview:_imageViewInCell];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

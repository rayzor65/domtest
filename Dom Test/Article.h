//
//  Article.h
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property int identifier;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * headLine;
@property (nonatomic, retain) NSString * slugLine;
@property (nonatomic, retain) NSString * thumbnailImageHref;
@property (nonatomic, retain) NSString * webHref;
@property (nonatomic, retain) NSString * tinyUrl;
@property (nonatomic, retain) NSString * dateLine;
@property BOOL imageLoaded;
@property (nonatomic, retain) UIImage * articleImage;

@end

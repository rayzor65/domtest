//
//  ArticleService.m
//  Dom Test
//
//  Created by raymond ho on 6/03/2014.
//  Copyright (c) 2014 Developer Ray. All rights reserved.
//

#import "ArticleService.h"
#import "Article.h"

@implementation ArticleService

- (void)fetchRawArticles
{
    NSString *apiUrlStr = @"http://mobilatr.mob.f2.com.au/services/views/9.json";
    NSURL *url = [[NSURL alloc] initWithString:apiUrlStr];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if ( ! error) {
            NSError *createError = nil;
            if ([_articles count] == 0) {
                _articles = [self createArticles:data error:&createError];
            } else {
                [_articles addObjectsFromArray:[self createArticles:data error:&createError]];
            }

            if (_articles) {
                [self postNotificationWithArticles:_articles];
                [self postNotificationArticlesFetched];
            } else {
                NSLog(@"There was an error creating the shows");
            }
        } else {
            NSLog(@"There was an error fetching the shows");
        }
    }];
}

- (NSMutableArray*)createArticles:(NSData *)data error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil && parsedObject != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *articles = [[NSMutableArray alloc] init];
    NSArray *results = [parsedObject valueForKey:@"items"];
    
    for (NSDictionary *dict in results) {
        Article *newArticle = [[Article alloc] init];
        newArticle.identifier = [[dict valueForKey:@"identifier"] intValue];
        newArticle.type = [dict valueForKey:@"type"];
        newArticle.headLine = [dict valueForKey:@"headLine"];
        newArticle.slugLine = [dict valueForKey:@"slugLine"];
        newArticle.thumbnailImageHref = [dict valueForKey:@"thumbnailImageHref"];
        newArticle.webHref = [dict valueForKey:@"webHref"];
        newArticle.tinyUrl = [dict valueForKey:@"tinyUrl"];
        newArticle.dateLine = [dict valueForKey:@"dateLine"];
        
        [articles addObject:newArticle];
    }
    
    return articles;
}

- (void)postNotificationWithArticles:(NSMutableArray*)articles
{
    NSString *notificationName = @"NewArticlesNotification";
    NSString *key = @"Articles";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:articles forKey:key];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (NSMutableArray *)getArticles
{
    return _articles;
}

- (UIImage *) getImageFromURL:(NSString *)fileURL
{
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}

- (void)postNotificationArticlesFetched
{
    NSString *notificationName = @"ArticlesFetched";
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:nil];
}
@end
